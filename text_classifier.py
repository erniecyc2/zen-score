# Copyright (c) Facebook, Inc. and its affiliates.
#
# This source code is licensed under the MIT license found in the
# LICENSE file in the root directory of this source tree.

import time
import copy
import math
import pdb
import logging
import random
import csv

from torch import Tensor
import torch
import torch.nn as nn
import torch.nn.functional as F
from torch.autograd import Variable
import torch.optim as optim

from torchtext.legacy.datasets import IMDB
from torchtext.data.utils import get_tokenizer
from torchtext.vocab import build_vocab_from_iterator
from torchtext.legacy import data

from nas.nas_arch import *
from nas.model_profiler import *

logger = logging.getLogger(__name__)


bptt = 1018

TEXT = data.Field(
    tokenize = 'spacy',
    tokenizer_language = 'en_core_web_sm',
    fix_length=bptt
)
LABEL = data.LabelField(dtype = torch.float)
train_data, test_data = IMDB.splits(TEXT, LABEL)
train_data, valid_data = train_data.split(random_state = random.seed(SEED))

print(f'Number of training examples: {len(train_data)}')
print(f'Number of validation examples: {len(valid_data)}')
print(f'Number of testing examples: {len(test_data)}')





# build vocab
MAX_VOCAB_SIZE = 25000
TEXT.build_vocab(train_data, max_size = MAX_VOCAB_SIZE)
LABEL.build_vocab(train_data)
print(f"Unique tokens in TEXT vocabulary: {len(TEXT.vocab)}")
print(f"Unique tokens in LABEL vocabulary: {len(LABEL.vocab)}")


BATCH_SIZE = 64

train_iter, valid_iter, test_iter = data.BucketIterator.splits(
    (train_data, valid_data, test_data),
    batch_size = BATCH_SIZE,
    device = device
)


def binary_accuracy(preds, y):
    """
    Returns accuracy per batch, i.e. if you get 8/10 right, this returns 0.8, NOT 8
    """

    #round predictions to the closest integer
    rounded_preds = torch.round(torch.sigmoid(preds))
    correct = (rounded_preds == y).float() #convert into float for division
    acc = correct.sum() / len(correct)
    return acc


input_dim = len(TEXT.vocab)
model_type = "transformer"
model = build_model(model_type=model_type, input_dim=input_dim, nlabels=1)
print(f'The model has {count_parameters(model):,} trainable parameters')

if model_type=="transformer":
    src_mask = model.generate_square_subsequent_mask(bptt).to(device)


optimizer = optim.Adam(model.parameters(), lr=1e-5)
criterion = nn.BCEWithLogitsLoss()




def train(model, iterator, optimizer, criterion):

    epoch_loss = 0
    epoch_acc = 0
    zen_scores = 0
    model.train()
    for i,batch in enumerate(iterator):
        optimizer.zero_grad()
        # import pdb; pdb.set_trace()
        if model_type=="transformer":
            predictions,zen_score = model(batch.text, src_mask, i)
        else:
            predictions = model(batch.text).squeeze(1)
            zen_score = torch.autograd.Variable(Tensor([0.]))
        loss = criterion(predictions, batch.label)
        acc = binary_accuracy(predictions, batch.label)
        loss.backward()
        optimizer.step()
        epoch_loss += loss.item()
        epoch_acc += acc.item()
        zen_scores += zen_score.item()

    return epoch_loss / len(iterator), epoch_acc / len(iterator), zen_scores / len(iterator)



def evaluate(model, iterator, criterion):

    epoch_loss = 0
    epoch_acc = 0
    model.eval()

    with torch.no_grad():
        for i,batch in enumerate(iterator):
            if model_type=="transformer":
                predictions,zen_score = model(batch.text, src_mask, i)
            else:
                predictions = model(batch.text).squeeze(1)
                zen_score = -1
            loss = criterion(predictions, batch.label)
            acc = binary_accuracy(predictions, batch.label)

            epoch_loss += loss.item()
            epoch_acc += acc.item()

    return epoch_loss / len(iterator), epoch_acc / len(iterator)



N_EPOCHS = 120
best_valid_loss = float('inf')

zen_scores_log = open('results/text_classification.csv', mode='w')
zen_writer = csv.writer(zen_scores_log, delimiter='\t', quotechar='"', quoting=csv.QUOTE_MINIMAL)


for epoch in range(N_EPOCHS):

    start_time = time.time()

    train_loss, train_acc, zen_score = train(model, train_iter, optimizer, criterion)
    valid_loss, valid_acc = evaluate(model, valid_iter, criterion)

    zen_writer.writerow([train_loss, train_acc, zen_score, valid_acc])

    end_time = time.time()

    epoch_mins, epoch_secs = epoch_time(start_time, end_time)

    if valid_loss < best_valid_loss:
        best_valid_loss = valid_loss
        torch.save(model.state_dict(), 'tut1-model.pt')

    print(f'Epoch: {epoch+1:02} | Epoch Time: {epoch_mins}m {epoch_secs}s')
    print(f'\tTrain Loss: {train_loss:.3f} | Train Acc: {train_acc*100:.2f}% | Zen Score: {zen_score:5.2f}')
    print(f'\t Val. Loss: {valid_loss:.3f} |  Val. Acc: {valid_acc*100:.2f}%')


model.load_state_dict(torch.load('tut1-model.pt'))

test_loss, test_acc = evaluate(model, test_iter, criterion)

print(f'Test Loss: {test_loss:.3f} | Test Acc: {test_acc*100:.2f}%')
